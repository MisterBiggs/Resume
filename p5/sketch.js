// Gravitational Attraction
// The Nature of Code
// The Coding Train / Daniel Shiffman
// https://youtu.be/EpgB3cNhKPM
// https://thecodingtrain.com/learning/nature-of-code/2.5-gravitational-attraction.html
// https://editor.p5js.org/codingtrain/sketches/MkLraatd

p5.disableFriendlyErrors = true;

let movers = [];
let attractor;
var canvas;

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function setup() {
  if (window.matchMedia("(prefers-reduced-motion: reduce)").matches) {
    console.log("Respecting reduced motion preference.");
    noLoop(); // Stops animation
    return;
  }

  canvas = createCanvas(windowWidth, windowHeight);
  canvas.position(0, 0);
  canvas.style("z-index", "-1000");

  const params = new URLSearchParams(window.location.search);

  if (params.has("gay")) {
    console.log('The "gay" parameter exists in the URL.');
    movers[0] = new Mover(50, "#D12229");
    movers[1] = new Mover(50, "#F68A1E");
    movers[2] = new Mover(50, "#FDE01A");
    movers[3] = new Mover(50, "#007940");
    movers[4] = new Mover(50, "#24408E");
    movers[5] = new Mover(50, "#732982");
  } else {
    movers[0] = new Mover(50, "red");
    movers[1] = new Mover(60, "purple");
    movers[2] = new Mover(40, "blue");
  }

  background(0);
}

function draw() {
  const isDarkMode = window.matchMedia("(prefers-color-scheme: dark)").matches;

  if (isDarkMode) {
    background(0, 25);
  } else {
    background(255, 25);
  }
  for (let mover of movers) {
    mover.mouse_mass();
    for (let other of movers) {
      if (mover !== other) {
        mover.attract(other);
      }
    }
  }
  for (let mover of movers) {
    mover.update();
    mover.edges();
    mover.show();
  }
}
