# Anson Biggs Resume

My personal resume website. The main splash page is made with p5.js, and the `/resume` endpoint is my personal resume which is made in simple html/css and even includes some fancy css to make the page print nicely. 

See it live at [ansonbiggs.com](https://ansonbiggs.com)